#pragma once
#include "Marco.h"
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/SparseCore>
#include <Eigen/Dense>
#include <string>
#include <vector>

namespace uni
{
	typedef int   Int;
	typedef long  Long;
	typedef bool  Bool;
	typedef unsigned int Uint;

#ifdef UNICODE
	typedef std::wstring String;
#else
	typedef std::string String;
#endif

	typedef String::value_type Char;
	typedef Char const * Lpsz;

#if UseDoublePrecision
	typedef double Float;
	typedef Eigen::VectorXd Vector;
	typedef Eigen::Vector4d Vec4;
	typedef Eigen::Vector3d Vec3;
	typedef Eigen::Vector2d Vec2;
	typedef Eigen::Matrix4d Mat4;
	typedef Eigen::Matrix3d Mat3;
	typedef Eigen::Matrix2d Mat2;
#else
	typedef float Float;
	typedef Eigen::VectorXf Vector;
	typedef Eigen::Vector4f Vec4;
	typedef Eigen::Vector3f Vec3;
	typedef Eigen::Vector2f Vec2;
	typedef Eigen::Matrix4f Mat4;
	typedef Eigen::Matrix3f Mat3;
	typedef Eigen::Matrix2f Mat2;
#endif // UseDoublePrecision
	typedef Eigen::VectorXi VectorI;
	typedef Eigen::Vector4i Vec4i;
	typedef Eigen::Vector3i Vec3i;
	typedef Eigen::Vector2i Vec2i;

}//namespace uni
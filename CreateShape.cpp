#include "CreatShape.h"
#include "Math.h"
#include <fstream>

namespace uni
{
	namespace Geometry
	{

		void MeshData::toObj(Lpsz path)
		{
			std::ofstream outfile(path);

			for each (Vertex var in Vertices)
			{
				outfile << "v " << var.Position.x() << " " << var.Position.y() << " " << var.Position.z() << std::endl;
				outfile << "vn " << var.Normal.x() << " " << var.Normal.y() << " " << var.Normal.z() << std::endl;
				outfile << "vt " << var.TexC.x() << " " << var.TexC.y() << std::endl;
			}

			for each (Vec3i var in Indices)
			{
				outfile << "f " << var.x() + 1  << "/" << var.x() + 1 << "/" << var.x() + 1 << " " 
					<< var.y() + 1 << "/" << var.y() + 1 << "/" << var.y() + 1 << " " 
					<< var.z() + 1 << "/" << var.z() + 1 << "/" << var.z() + 1 << std::endl;
			}

			outfile.close();
		}

		MeshData CreateBox(Float width, Float height, Float depth)
		{
			MeshData ret;

			Float const w2 = 0.5*width;
			Float const h2 = 0.5*height;
			Float const d2 = 0.5*depth;
			// front
			ret.Vertices.push_back(Vertex(-w2, -h2, -d2, 0, 0, -1, 1, 0, 0, 0, 1));
			ret.Vertices.push_back(Vertex(-w2, +h2, -d2, 0, 0, -1, 1, 0, 0, 0, 0));
			ret.Vertices.push_back(Vertex(+w2, +h2, -d2, 0, 0, -1, 1, 0, 0, 1, 0));
			ret.Vertices.push_back(Vertex(+w2, -h2, -d2, 0, 0, -1, 1, 0, 0, 1, 1));
			// back
			ret.Vertices.push_back(Vertex(-w2, -h2, +d2, 0, 0, 1, -1, 0, 0, 1, 1));
			ret.Vertices.push_back(Vertex(+w2, -h2, +d2, 0, 0, 1, -1, 0, 0, 0, 1));
			ret.Vertices.push_back(Vertex(+w2, +h2, +d2, 0, 0, 1, -1, 0, 0, 0, 0));
			ret.Vertices.push_back(Vertex(-w2, +h2, +d2, 0, 0, 1, -1, 0, 0, 1, 0));
			// top
			ret.Vertices.push_back(Vertex(-w2, +h2, -d2, 0, 1, 0, 1, 0, 0, 0, 1));
			ret.Vertices.push_back(Vertex(-w2, +h2, +d2, 0, 1, 0, 1, 0, 0, 0, 0));
			ret.Vertices.push_back(Vertex(+w2, +h2, +d2, 0, 1, 0, 1, 0, 0, 1, 0));
			ret.Vertices.push_back(Vertex(+w2, +h2, -d2, 0, 1, 0, 1, 0, 0, 1, 1));
			// bottom
			ret.Vertices.push_back(Vertex(-w2, -h2, -d2, 0, -1, 0, -1, 0, 0, 1, 1));
			ret.Vertices.push_back(Vertex(+w2, -h2, -d2, 0, -1, 0, -1, 0, 0, 0, 1));
			ret.Vertices.push_back(Vertex(+w2, -h2, +d2, 0, -1, 0, -1, 0, 0, 0, 0));
			ret.Vertices.push_back(Vertex(-w2, -h2, +d2, 0, -1, 0, -1, 0, 0, 1, 0));
			// left
			ret.Vertices.push_back(Vertex(-w2, -h2, +d2, -1, 0, 0, 0, 0, -1, 0, 1));
			ret.Vertices.push_back(Vertex(-w2, +h2, +d2, -1, 0, 0, 0, 0, -1, 0, 0));
			ret.Vertices.push_back(Vertex(-w2, +h2, -d2, -1, 0, 0, 0, 0, -1, 1, 0));
			ret.Vertices.push_back(Vertex(-w2, -h2, -d2, -1, 0, 0, 0, 0, -1, 1, 1));
			// right
			ret.Vertices.push_back(Vertex(+w2, -h2, -d2, 1, 0, 0, 0, 0, 1, 0, 1));
			ret.Vertices.push_back(Vertex(+w2, +h2, -d2, 1, 0, 0, 0, 0, 1, 0, 0));
			ret.Vertices.push_back(Vertex(+w2, +h2, +d2, 1, 0, 0, 0, 0, 1, 1, 0));
			ret.Vertices.push_back(Vertex(+w2, -h2, +d2, 1, 0, 0, 0, 0, 1, 1, 1));

			Int range[] = { 0, 1, 2, 0, 2, 3,
				4, 5, 6, 4, 6, 7,
				8, 9, 10, 8, 10, 11,
				12, 13, 14, 12, 14, 15,
				16, 17, 18, 16, 18, 19,
				20, 21, 22, 20, 22, 23 };

			const Int nTriSize = sizeof(range) / sizeof(range[0]);
			for (size_t i = 0; i < nTriSize; i += 3)
			{
				ret.Indices.push_back(Vec3i(range[i + 0], range[i + 1] , range[i + 2] ));
			}

			return ret;
		}//MeshData CreateBox(Float width, Float height, Float depth)

		MeshData CreateSphere(Float radius, Int sliceCount, Int stackCount)
		{
			MeshData ret;
			ret.Vertices.push_back( Vertex(0, radius, 0, 0, 1, 0, 1, 0, 0, 0, 0));
			Float phiStep = Math::PI() / (Float)stackCount;
			Float thetaStep = 2.0*Math::PI() / (Float)sliceCount;

			for (int i = 1; i <= (stackCount - 1); i++) 
			{
				Float phi = i*phiStep;
				for (int j = 0; j <= sliceCount; j++) 
				{
					Float theta = j*thetaStep;
					Vec3 p(
						(radius*Math::Sin(phi)*Math::Cos(theta)),
						(radius*Math::Cos(phi)),
						(radius* Math::Sin(phi)*Math::Sin(theta))
					);

					Vec3 t(-radius*Math::Sin(phi)*Math::Sin(theta), 0, radius*Math::Sin(phi)*Math::Cos(theta));
					t.normalize();
					Vec3 n = p;
					n.normalize();
					Vec2 uv (theta / (Math::PI() * 2), phi / Math::PI());
					ret.Vertices.push_back( Vertex(p, n, t, uv));
				}
			}
			ret.Vertices.push_back( Vertex(0, -radius, 0, 0, -1, 0, 1, 0, 0, 0, 1));


			for (int i = 1; i <= sliceCount; i++) {
				ret.Indices.push_back(Vec3i(0, i + 1, i));
				/*ret.Indices.push_back(0);
				ret.Indices.push_back(i + 1);
				ret.Indices.push_back(i);*/
			}
			Int baseIndex = 1;
			Int ringVertexCount = sliceCount + 1;
			for (int i = 0; i < stackCount - 2; i++) {
				for (int j = 0; j < sliceCount; j++) {
					ret.Indices.push_back(
						Vec3i(baseIndex + i*ringVertexCount + j, 
						      baseIndex + i*ringVertexCount + j + 1, 
							  baseIndex + (i + 1)*ringVertexCount + j));
					/*ret.Indices.push_back(baseIndex + i*ringVertexCount + j);
					ret.Indices.push_back(baseIndex + i*ringVertexCount + j + 1);
					ret.Indices.push_back(baseIndex + (i + 1)*ringVertexCount + j);*/
					ret.Indices.push_back(
						Vec3i(baseIndex + (i + 1)*ringVertexCount + j,
							baseIndex + i*ringVertexCount + j + 1,
							baseIndex + (i + 1)*ringVertexCount + j + 1));
					/*ret.Indices.push_back(baseIndex + (i + 1)*ringVertexCount + j);
					ret.Indices.push_back(baseIndex + i*ringVertexCount + j + 1);
					ret.Indices.push_back(baseIndex + (i + 1)*ringVertexCount + j + 1);*/
				}
			}
			Int southPoleIndex = ret.Vertices.size() - 1;
			baseIndex = southPoleIndex - ringVertexCount;
			for (int i = 0; i < sliceCount; i++) {
				ret.Indices.push_back(Vec3i(southPoleIndex, baseIndex + i, baseIndex + i + 1));
				/*ret.Indices.push_back(southPoleIndex);
				ret.Indices.push_back(baseIndex + i);
				ret.Indices.push_back(baseIndex + i + 1);*/
			}
			return ret;
		}

		void BuildCylinderTopCap(Float topRadius, Float height, Int sliceCount, MeshData& ret)
		{
			const Int baseIndex = ret.Vertices.size();

			const Float y = 0.5*height;
			const Float dTheta = 2.0*Math::PI() / sliceCount;

			for (int i = 0; i <= sliceCount; i++) {
				const Float x = topRadius*Math::Cos(i*dTheta);
				const Float z = topRadius*Math::Sin(i*dTheta);

				const Float u = x / height + 0.5;
				const Float v = z / height + 0.5;
				ret.Vertices.push_back( Vertex(x, y, z, 0, 1, 0, 1, 0, 0, u, v));
			}
			ret.Vertices.push_back( Vertex(0, y, 0, 0, 1, 0, 1, 0, 0, 0.5f, 0.5f));
			const Int centerIndex = ret.Vertices.size() - 1;
			for (int i = 0; i < sliceCount; i++) {
				ret.Indices.push_back( Vec3i(centerIndex, baseIndex + i + 1, baseIndex + i) );
				/*ret.Indices.Add(centerIndex);
				ret.Indices.Add(baseIndex + i + 1);
				ret.Indices.Add(baseIndex + i);*/
			}
		}

		void BuildCylinderBottomCap(Float bottomRadius, Float height, Int sliceCount, MeshData& ret)
		{
			const Int baseIndex = ret.Vertices.size();

			const Float y = -0.5 * height;
			const Float dTheta = 2.0 * Math::PI() / sliceCount;

			for (int i = 0; i <= sliceCount; i++) {
				const Float x = bottomRadius * Math::Cos(i * dTheta);
				const Float z = bottomRadius * Math::Sin(i * dTheta);

				const Float u = x / height + 0.5;
				const Float v = z / height + 0.5;
				ret.Vertices.push_back(Vertex(x, y, z, 0, -1, 0, 1, 0, 0, u, v));
			}
			ret.Vertices.push_back( Vertex(0, y, 0, 0, -1, 0, 1, 0, 0, 0.5f, 0.5f));
			const Int centerIndex = ret.Vertices.size() - 1;
			for (int i = 0; i < sliceCount; i++) 
			{
				ret.Indices.push_back( Vec3i(centerIndex, baseIndex + i, baseIndex + i + 1) );
				/*ret.Indices.Add(centerIndex);
				ret.Indices.Add(baseIndex + i);
				ret.Indices.Add(baseIndex + i + 1);*/
			}
		}

		MeshData CreateCylinder(Float bottomRadius, Float topRadius, Float height, Int sliceCount, Int stackCount)
		{
			MeshData ret;
			const Float stackHeight = height / stackCount;
			const Float radiusStep = (topRadius - bottomRadius) / stackCount;
			const Int ringCount = stackCount + 1;

			for (int i = 0; i < ringCount; i++) {
				const Float y = -0.5*height + i*stackHeight;
				const Float r = bottomRadius + i*radiusStep;
				const Float dTheta = 2.0 * Math::PI() / sliceCount;
				for (int j = 0; j <= sliceCount; j++) {

					const Float c = Math::Cos(j*dTheta);
					const Float s = Math::Sin(j*dTheta);

					const Vec3 v (r*c, y, r*s);
					const Vec2 uv ((Float)j / sliceCount, 1.0 - (Float)i / stackCount);
					const Vec3 t (-s, 0.0f, c);

					const Float dr = bottomRadius - topRadius;
					const Vec3 bitangent (dr*c, -height, dr*s);

					Vec3 n = t.cross(bitangent); //= Vector3.Cross(t, bitangent);
					n.normalize();
					ret.Vertices.push_back( Vertex(v, n, t, uv));

				}
			}
			const Int ringVertexCount = sliceCount + 1;
			for (int i = 0; i < stackCount; i++) {
				for (int j = 0; j < sliceCount; j++) {
					ret.Indices.push_back(Vec3i(i*ringVertexCount + j, (i + 1)*ringVertexCount + j, (i + 1)*ringVertexCount + j + 1));
					/*ret.Indices.Add(i*ringVertexCount + j);
					ret.Indices.Add((i + 1)*ringVertexCount + j);
					ret.Indices.Add((i + 1)*ringVertexCount + j + 1);*/

					ret.Indices.push_back(Vec3i(i*ringVertexCount + j, (i + 1)*ringVertexCount + j + 1, i*ringVertexCount + j + 1));
					/*ret.Indices.Add(i*ringVertexCount + j);
					ret.Indices.Add((i + 1)*ringVertexCount + j + 1);
					ret.Indices.Add(i*ringVertexCount + j + 1);*/
				}
			}
			BuildCylinderTopCap(topRadius, height, sliceCount, ret);
			BuildCylinderBottomCap(bottomRadius, height, sliceCount, ret);
			return ret;
		}

	}//namespace Geometry
	
}//namespace uni
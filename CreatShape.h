#pragma once
#include "Types.h"

namespace uni
{
	namespace Geometry
	{
		struct Vertex
		{
			Vertex(Float px, Float py, Float pz, Float nx, Float ny, Float nz, Float tx, Float ty, Float tz, Float u, Float v)
				: Position(px, py, pz), Normal(nx, ny, nz), TangentU(tx, ty, tz), TexC(u, v)
			{}

			Vertex(Vec3 const & pos, Vec3 const & norm, Vec3 const & tan, Vec2 const & uv)
				:Position(pos), Normal(norm), TangentU(tan), TexC(uv)
			{}

			Vec3 Position;
			Vec3 Normal;
			Vec3 TangentU;
			Vec2 TexC;

		};

		struct MeshData
		{
			void toObj(Lpsz path);
			std::vector< Vertex > Vertices;
			std::vector<Vec3i> Indices;
		};

		MeshData CreateBox(Float width, Float height, Float depth);

		MeshData CreateSphere(Float radius, Int sliceCount, Int stackCount);

		MeshData CreateCylinder(Float bottomRadius, Float topRadius, Float height, Int sliceCount, Int stackCount);

	}//namespace Geometry
}//namespace uni
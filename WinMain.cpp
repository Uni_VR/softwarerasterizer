#include <windows.h>
#include "Types.h"
#include "CreatShape.h"
#include "Log.h"
#include <locale>
#include <codecvt>//wstring_convert
#include <iomanip>
#include "UtilityFuncs.h"
#include "Configure\config.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	uni::UtilityFuncs::EnvCheck();
	LPWSTR *szArgList;
	int argCount;

	szArgList = CommandLineToArgvW(GetCommandLineW(), &argCount);
	if (szArgList == NULL)
	{
		LogInfo(__TEXT("Unable to parse command line"));
		return -10;
	}
	FILELog::ReportingLevel() = FILELog::FromString("DEBUG1");
	FILE* log_fd = fopen("logfile.txt", "w");
	Output2FILE::Stream() = log_fd;

	uniAssert(2 == argCount);

	std::wstring_convert<std::codecvt_utf8<wchar_t>> conv1;
	std::string u8str = conv1.to_bytes(szArgList[1]);
	FILE_LOG(logDEBUG) << "Configure File : " << u8str;

	LocalFree(szArgList);

	Config config(u8str.c_str());

	// basic usage of properties
	if (config.pBool("showHello")) {
		int cnt = config.pInt("helloCount");
		string msg = config.pString("helloMessage");
		for (int i = 0; i < cnt; ++i) {
			FILE_LOG(logDEBUG) << msg << endl;
		}
		FILE_LOG(logDEBUG) << endl;
	}

	// properties with expanded names (no difference in using)
	FILE_LOG(logDEBUG) << "tempFolder    = '" << config.pString("tempFolder") << "'" << endl;
	FILE_LOG(logDEBUG) << "tempSubFolder = '" << config.pString("tempSubFolder") << "'" << endl;
	FILE_LOG(logDEBUG) << endl;

	// get properties for all subgroups starting with prefix
	map<string, Config*> messages = config.getGroups(); // all groups
	const string messagePrefix = "message"; // prefix for group name
	for (map<string, Config*>::iterator i = messages.begin(); i != messages.end(); ++i) {
		string groupName = i->first;
		Config* group = i->second;

		// test group name for prefix
		if (groupName.substr(0, messagePrefix.length()) == messagePrefix) {
			// display group contents
			FILE_LOG(logDEBUG) << group->pString("name") << ":" << endl;
			FILE_LOG(logDEBUG) << "   " << group->pString("text") << endl;
		}
	}
	
	return 0;
}
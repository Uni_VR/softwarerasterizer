#include "UtilityFuncs.h"
#include <time.h>
#include <sstream>
#include <codecvt>//wstring_convert

namespace uni
{
	namespace UtilityFuncs
	{
		std::string WStr2Str(std::wstring const& wstr)
		{
			std::wstring_convert<std::codecvt_utf8<wchar_t>> conv;
			return conv.to_bytes(wstr.c_str());
		}

		String TimeStamp()
		{
#if _UNICODE
			static std::wstringstream ss;
#else
			static std::stringstream ss;
#endif
			
			ss.clear();
#if defined(ENV64BIT)
			ss << (unsigned long)time(NULL);
#elif defined (ENV32BIT)
			ss << (unsigned)time(NULL);
#else
#error "Must define either ENV32BIT or ENV64BIT"
#endif
			return ss.str();
		}

		Bool EnvCheck()
		{
#if defined(ENV64BIT)
			if (sizeof(void*) != 8)
			{
				wprintf(L"ENV64BIT: Error: pointer should be 8 bytes. Exiting.");
				exit(0);
			}
			wprintf(L"Diagnostics: we are running in 64-bit mode.\n");
#elif defined (ENV32BIT)
			if (sizeof(void*) != 4)
			{
				wprintf(L"ENV32BIT: Error: pointer should be 4 bytes. Exiting.");
				exit(0);
			}
			wprintf(L"Diagnostics: we are running in 32-bit mode.\n");
#else
			#error "Must define either ENV32BIT or ENV64BIT".
#endif
			return uniTrue;
		}//func EnvCheck

		void SampleFunc()
		{
#if defined(ENV64BIT)
			// 64-bit code here.
#elif defined (ENV32BIT)
			// 32-bit code here.
#else
			// INCREASE ROBUSTNESS. ALWAYS THROW AN ERROR ON THE ELSE.
			// - What if I made a typo and checked for ENV6BIT instead of ENV64BIT?
			// - What if both ENV64BIT and ENV32BIT are not defined?
			// - What if project is corrupted, and _WIN64 and _WIN32 are not defined?
			// - What if I didn't include the required header file?
			// - What if I checked for _WIN32 first instead of second?
			//   (in Windows, both are defined in 64-bit, so this will break codebase)
			// - What if the code has just been ported to a different OS?
			// - What if there is an unknown unknown, not mentioned in this list so far?
			// I'm only human, and the mistakes above would break the *entire* codebase.
			#error "Must define either ENV32BIT or ENV64BIT"
#endif
		}
	}//namespace UtilityFuncs
}//namespace uni
#pragma once
#include "Types.h"

namespace uni
{
	namespace Math
	{
		inline Float PI(){ return 3.1415926535898; }
		inline Float DegreeToRadian(Float degree) { return (degree) * (PI() / 180.0); }
		inline Float Sin(Float radian) { return sin(radian); }
		inline Float Cos(Float radian) { return cos(radian); }
	}
}//namespace uni
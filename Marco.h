#pragma once

// Check windows
#if _WIN32 || _WIN64
#if _WIN64
#define ENV64BIT
#else
#define ENV32BIT
#endif
#endif

// Check GCC
#if __GNUC__
#if __x86_64__ || __ppc64__
#define ENV64BIT
#else
#define ENV32BIT
#endif
#endif

#define UseDoublePrecision (1)
#define uniNoticeMsg(__msg__)
#define LogInfo(__nu__) OutputDebugString(__nu__)
#define VNAME(name) (#name)
#define uniExit exit(66)
#define uniSuccess (0)
#define uniFail (-1)
#define uniAssert(X) do{if(!X){LogInfo(__TEXT(#X));system("pause");}}while(false)
#define uniZero (0)
#define uniOne (1)
#define uniTrue (true)
#define uniFalse (false)


#pragma once
#include "Types.h"
namespace uni
{
	namespace UtilityFuncs
	{
		Bool EnvCheck();
		String TimeStamp();
		void SampleFunc();
	}//namespace UtilityFuncs
}//namespace uni